// This document contains our app feature in displaying and manipulating our database

const Task = require("../models/task.js");

module.exports.getAllTask = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name : requestBody.name 
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false; //"Error detected"
		}
		else{
			return task;
		}
	})
}