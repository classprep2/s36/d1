// This document contains all the endpoints for our application (also http methods)

const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskContoller.js");

router.get("/viewTasks", (req, res) =>{
	// Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
							//returns result from our controller
	taskController.getAllTask().then(result => res.send(result));
})

router.post("/addNewTask" , (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})


module.exports = router;